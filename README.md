<h1>The activity agenda</h1>
<h4>Maarten Stolk - 0882911</h4>

<strong>Description</strong><br />
For my study i had to make an activity agenda based on the API of NEMO kennislink.<br />
This project shows the provinces with the number of actvities that are available<br />
in that province based on the audience you selected.

<strong>How to get it to work</strong>
1. Start your terminal
2. navigate to the folder where you want to dl this project
3. type: git clone https://gitlab.com/proteinslurp/fed-herkansing.git
4. type: npm install
5. Go to the folder and open the index.html in your browser!

<strong>How to develop</strong>
1. Start your terminal
2. navigate to the folder where you want to dl this project
3. type: git clone https://gitlab.com/proteinslurp/fed-herkansing.git
4. type: cd fed-herkansing
5. type: npm install
6. type: npm run babel:watch:js