// Global variables
let provinceActivities;
let provinceGallery;
var audience = 1; // Default 1 are the children

/**
 * Initialize the application (after DOM ready)
 */
var init = () =>
{
    getAllProvinces();
    provinceGallery = document.getElementById('provinces');
    provinceGallery.addEventListener('click', movieCardClickHandler);

    getAllAudiences();  
};

/**
 *  The sortChanged function checks if the value of
 *  the sort box has changed. If so he empty's the content
 *  And gets the activities and provinces again based on the
 *  new sort value.
 */
var sortChanged = () =>
{
    // Get the sorter based on id
    audienceSorter = document.getElementById('sorter');
    // Get the value from the sorter
    SelectedValue = audienceSorter.options[audienceSorter.selectedIndex].value;

    // Safe the value of sorter in audience
    audience = SelectedValue;
    
    // Clear the html of both elements
    items = document.getElementById('items');
    provinceGallery.innerHTML = "";
    items.innerHTML = "";
    
    // Get all provinces again
    getAllProvinces();
};  

/**
 *  The getAllProvinces function loops trough each item
 *  in the array provinces and does for each province an
 *  AJAX call with the ID of that province
 */
var getAllProvinces = () =>
{
    // Array with all provinces
    let provinces = [
        {
            name: 'Drenthe',
            id: 1
        }, {
            name: 'Flevoland',
            id: 11
        }, {
            name: 'Friesland',
            id: 21
        }, {
            name: 'Gelderland',
            id: 31
        }, {
            name: 'Groningen',
            id: 41
        }, {
            name: 'Limburg',
            id: 51
        }, {
            name: 'Noord-Brabant',
            id: 61
        }, {
            name: 'Noord-Holland',
            id: 71
        }, {
            name: 'Overijssel',
            id: 111
        }, {
            name: 'Utrecht',
            id: 81
        }, {
            name: 'Zeeland',
            id: 91
        }, {
            name: 'Zuid-Holland',
            id: 101
        }
    ];

    // For each province in the array provinces
    for (let province of provinces) {
        // Start the function with the id
        let provinceID = province.id;
        getAllActivities(provinceID);
    }
};

/**
 *  The getAllAudiences function loops trough each item
 *  in the array audiences and adds a option to the select
 *  box for each audience
 */
var getAllAudiences = () =>
{
    // Array with all audiences
    let audiences = [
        {
            name: 'Kinderen',
            id: 1
        }, {
            name: 'Jonge kinderen',
            id: 0
        }, {
            name: 'Jongeren',
            id: 11
        }, {
            name: 'Volwassenen',
            id: 21
        }
    ];

    // Get the sorter based on id
    let audienceSorter = document.getElementById('sorter');

    // For each audience
    for (let audience of audiences) {
        // Safe the name and id in a variable
        audienceName = audience.name;
        audienceID = audience.id;

        // Create an option element with the right value and content
        let sortCategory = createDomElement({tagName: 'option', attributes: {value: audienceID}, content: audienceName});

        // Append it to the sort function
        audienceSorter.appendChild(sortCategory);

    }   
};

/**
 *  The getAllActivities function does an ajax request with
 *  a variable provinceID and audience id. When its succesvol
 *  We start the function showActivities
 */
var getAllActivities = (provinceID) =>
{
    ajaxRequest("https://www.nemokennislink.nl/api/activiteiten.json?sleutel=cgwr10yo3h&per_page=20&province=" + provinceID + "&audience=" + audience, showActivities);
};

/**
 * Generic AJAX handler (to prevent reqwest everywhere)
 *
 * @param url
 * @param ajaxSuccessHandler
 * @param [data]
 */
var ajaxRequest = (url, ajaxSuccessHandler, data) =>
{
    //Default ajax parameters
    let parameters = {
        type: 'jsonp',
        url: url,
        success: ajaxSuccessHandler
    };

    //If data is passed, add it to the AJAX parameters
    if (data) {
        parameters.data = data;
    }

    //Actual AJAX call
    reqwest(parameters);
};

/**
 *  The showActivities function has al the results from the
 *  ajax request and creates elements for each result.
 */
var showActivities = (data) =>
{   
    // Safe the data in variables
    provinceActivities = data.results;
    provinceName = data.results[0].provinces[0].name;
    totalActivities = data.count;
    provinceNumber = data.results[0].provinces[0].id;
    
    // Create a div with the class card c
    let card = createDomElement({tagName: 'div', attributes: {class: 'card c' + provinceNumber }});

    //Create all children HTML tags for the card
    let title = createDomElement({tagName: 'h4', attributes: {class: 'title'}, content: provinceName + ' (' + totalActivities + ')'});
    let actorLink = createDomElement({tagName: 'a', attributes: {class: 'load-actors show-actors', href: '#', id: 'card-' + provinceNumber}, content: 'Toon activiteiten'});

    // Append all elements to the card
    card.appendChild(title);
    card.appendChild(actorLink);

    //Append the card to the gallery
    provinceGallery.appendChild(card);

};

/**
 * Handle the click on the card
 *
 * @param e
 */
var movieCardClickHandler = (e) =>
{
    // Prevent default behavior & save target in local variable
    e.preventDefault();
    let target = e.target;
    
    // Clear the html of both elements
    items = document.getElementById('items');
    items.innerHTML = "";

    // Create a list for an item
    let list = createDomElement({tagName: 'ul', attributes: {class: 'activities'}});

    // Get the item div and append the list to it
    items = document.getElementById('items');
    items.appendChild(list);

    // Remove the class from the target
    let index = parseInt(target.id.replace('card-', ''));

    // Ajax request to get the activities from that province+audience
    ajaxRequest("https://www.nemokennislink.nl/api/activiteiten.json?sleutel=cgwr10yo3h&per_page=20&province=" + index + "&audience=" + audience, function(data){
        
        // Store the results in a variable
        let movie = data.results;

        // For each activity in the results we create content
        // and append it to the list
        for (let activity of movie) {
            let activityName = activity.title;
            let activitySubtitle = activity.subtitle;
            let activityPre = activity.lead;
            let activityUrl = activity.url;

            let li = createDomElement({tagName: 'li', attributes: {class: 'single-activity'}});

            let title = createDomElement({tagName: 'p', attributes: {class: 'title'}, content: activityName});
            let subtitle = createDomElement({tagName: 'p', attributes: {class: 'subtitle'}, content: activitySubtitle});
            let pre = createDomElement({tagName: 'p', attributes: {class: 'pre'}, content: activityPre});
            let url = createDomElement({tagName: 'a', attributes: {class: 'subtitle', href: activityUrl}, content: 'Bekijk Activiteit'});
            
            li.appendChild(title);
            li.appendChild(subtitle);
            li.appendChild(pre);
            li.appendChild(url);

            list.appendChild(li);
        }
    });

};

/**
 * Generic function to create new DOM elements
 *
 * @param properties
 * @returns {Element}
 */
var createDomElement = (properties) =>
{
    //Create the element
    let domElement = document.createElement(properties.tagName);

    //Loop through the attributes to set them on the element
    let attributes = properties.attributes;
    for (let prop in attributes) {
        domElement.setAttribute(prop, attributes[prop]);
    }

    //If any content, set the inner HTML
    if (properties.content) {
        domElement.innerHTML = properties.content;
    }

    //Return to use in other functions
    return domElement;
};

//Actual DOM ready handler
window.addEventListener("load", init);
